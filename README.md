# Dynamic Form Fields

## Getting Started

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


<p align="center">
  <img alt="projeto Dynamic Form Fields" src=".github/dynamic-form-fields.png" width="100%">
</p>