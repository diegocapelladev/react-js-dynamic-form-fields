import { useState } from 'react'
import { Button, Container, TextField } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import Remove from '@mui/icons-material/Remove'
import SendIcon from '@mui/icons-material/Send'

import './App.css'

function App() {
  const [inputFields, setInputFields] = useState([
    {firstName: '', lastName: ''}
  ])

  const handleChangeInput = (index, event) => {
    

    const values = [...inputFields]

    values[index][event.target.name] = event.target.value
    
    setInputFields(values)
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    console.log(inputFields)
  }

  const handleAddField = () => {
    setInputFields([...inputFields, {firstName: '', lastName: ''}])
  }

  const handleRemoveField = (index) => {
    const fields = [...inputFields]

    fields.splice(index, 1)

    setInputFields(fields)
  }

  return (
    <Container>
      <h1>Add New Member</h1>

      <form 
        style={{ 
          display: 'flex', 
          flexDirection: 'column', 
          alignItems: 'start' 
        }}
        onSubmit={handleSubmit}
      >
        {
          inputFields.map((field, index) => (
            <div key={index} style={{ display: 'flex', gap: '2rem', marginBottom: '1rem' }}>
              <TextField 
                label="First Name" 
                name="firstName" 
                value={field.firstName}
                onChange={(event) => handleChangeInput(index, event)}
                variant="outlined" 
              />
              <TextField 
                label="Last Name" 
                name="lastName" 
                value={field.lastName}
                onChange={(event) => handleChangeInput(index, event)}
                variant="outlined" 
              />
              
              <Button color="primary" aria-label="add" onClick={() => handleRemoveField()}>
                <Remove />
              </Button>

              <Button color="primary" aria-label="add" onClick={() => handleAddField(index)}>
                <AddIcon />
              </Button>
            </div>
          ))
        }

        <Button 
          style={{ marginTop: '2rem' }} 
          variant="contained" 
          endIcon={<SendIcon />}
          onClick={handleSubmit}
        >
          Send
        </Button>
      </form>
    </Container>
  )
}

export default App
